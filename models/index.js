
import mongodb from 'mongodb';

let {MongoClient} = mongodb;

import buildQuiz from './quiz.js';
import buildAnswer from './answer.js';

export default async () => {
    console.log(process.env.MONGODB_URI);
    
    const client = await MongoClient.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
    const db = client.db(process.env.MONGODB_DB_NAME);
    const collections = {
        Quizs: db.collection('Quizs'),
        Answers: db.collection('Answers'),
    };


    return {
        _collections: collections,
        Quiz: buildQuiz(collections),
        Answer: buildAnswer(collections)
    };
};
  