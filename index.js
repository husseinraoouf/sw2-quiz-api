import dotenv from 'dotenv';

dotenv.config();

import QuizController from './controllers/QuizController.js';
import AnswersController from './controllers/AnswersController.js';

import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import connectDB from './models/index.js';

const start = async () => {
    const app = express();
    app.use(bodyParser.json());
    app.use(cors());
    
    const dbModel = await connectDB();
    
    QuizController(app, dbModel);
    AnswersController(app, dbModel);
    
    const port = process.env.PORT || 3000;
    app.listen(port, () => {
        console.log("Listennig to port: " + port);    
    })
}

start();