export default (app, { Answer }) => {

    app.get('/socre/:userId', async (req, res) => {
        const socres = await Answer.getAllUserScores(req.params.userId);
        res.json(socres);
    });

    app.get('/socre/:userId/:quizId', async (req, res) => {
        const socre = await Answer.getUserScoreInQuiz(req.params.userId, req.params.quizId);
        res.json(socre);
    });

    app.post('/quiz/:quizId/asnwer', async (req, res) => {
        const answer = await Answer.add(req.params.quizId, req.body);
        res.json(answer);
    });

}