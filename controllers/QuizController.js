export default (app, { Quiz }) => {
    app.get('/quiz', async (req, res) => {
        const quizes = await Quiz.all();
        res.json(quizes);
    });

    app.get('/quiz/:quizId', async (req, res) => {
        const quiz = await Quiz.get(req.params.quizId);
        res.json(quiz);
    });

    app.post('/quiz', async (req, res) => {
        const quiz = await Quiz.add(req.body);
        res.json(quiz);
    });

    app.delete('/quiz/:quizId', async (req, res) => {
        const result = await Quiz.delete(req.params.quizId);
        res.json(result);
    });
}